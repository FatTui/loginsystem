<?php    
    include_once $_SERVER['DOCUMENT_ROOT'] . '/repository/loginsystem/includes/helpers.inc.php';
    require_once $_SERVER['DOCUMENT_ROOT'] . '/repository/loginsystem/includes/access.inc.php';
    //echo $_SESSION['working'];
?>
<body>
    <div class="grid-container full">
        <div class="content-spacer"></div>
        <div class="grid-container padded-grid">
            <div class="grid-x grid-margin-x first-grid">
                <div class="login-container small-12 medium-8 large-6 cell">
                    <h1>Login</h1>
                    <?php if (isset($loginError)): ?>
                        <p class="text"><?php echo $_SESSION['loginError']; ?></p>
                    <?php endif; ?> 
                    <?php
                        if (userIsLoggedIn())
                        {
                            echo '
                            <div class="login-status">
                                <p>You are currently logged in.</p>
                                <form method="post" action="" class="logout-form">
                                    <input type="hidden" name="action" value="log out">
                                    <input type="submit" class="logout-button" value="logout">
                                </form>
                            </div>';
                        }
                    ?>
                    <form method="post" action="">
                        <label class="email-input-label">E-mail</label>
                        <div class="input-wrap email-field-wrap">
                            <input required type="text" name="email" id="email" class="user-input email-input" placeholder="Your email here...">
                        </div>
                        <label class="pwd-input-label">Password</label>
                        <div class="input-wrap password-field-wrap">
                            <input required type="password" name="password" id="pwd1" class="user-input password-input" placeholder="Your password here...">
                        </div>
                        <div class="remember-me-wrap">
                            <div class="pretty p-svg p-curve">
                                <input type="checkbox" />
                                <div class="state p-success">
                                    <!-- svg path -->
                                    <svg class="svg svg-icon" viewBox="0 0 20 20">
                                        <path d="M7.629,14.566c0.125,0.125,0.291,0.188,0.456,0.188c0.164,0,0.329-0.062,0.456-0.188l8.219-8.221c0.252-0.252,0.252-0.659,0-0.911c-0.252-0.252-0.659-0.252-0.911,0l-7.764,7.763L4.152,9.267c-0.252-0.251-0.66-0.251-0.911,0c-0.252,0.252-0.252,0.66,0,0.911L7.629,14.566z" style="stroke: white;fill:white;"></path>
                                    </svg>
                                    <label>Keep me signed in</label>
                                </div>
                            </div>

                        </div>
                        <div class="submit-wrap">
                            <input type="hidden" name="action" value="log in">
                            <input type="submit" class="login-button" id="login-button" value="Login">
                            <a href="./signup.php" class="register-button"><p>register</p></a>
                        </div>
                        <div class="forgot-password-wrap">
                            <a href="#" class="forgot-password-link">Forgot your password?</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>



