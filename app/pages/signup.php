<body>
    <div class="grid-container full">
        <div class="content-spacer"></div>
        <div class="grid-container padded-grid">
            <div class="grid-x grid-margin-x first-grid">
                <div class="login-container small-12 medium-8 large-6 cell">
                    <h1>Signup</h1>
                    <form method="post" action="">
                        <label class="email-input-label">E-mail</label>
                        <div class="input-wrap email-field-wrap">
                            <input required type="text" name="email" id="email" class="user-input email-input <?php if (isset($_SESSION['emailErrorInvalid']) || isset($_SESSION['emailErrorInUse'])): ?>invalid-input<?php endif; ?>" placeholder="Your email here...">
                            <div class="icon-wrap">
                                <i id="email-icon-cross" class="inactive-cross-icon fas fa-exclamation-circle"></i>
                            </div>
                            <div class="icon-wrap">
                                <i id="email-icon-check" class="inactive-check-icon fas fa-check"></i>
                            </div>
                            <?php if (isset($_SESSION['emailErrorInvalid']) || isset($_SESSION['emailErrorInUse'])): ?>
                            <div class="icon-wrap">
                                <i id="email-icon-cross" class="invalid-input-icon fas fa-exclamation-circle"></i>
                            </div>
                            <?php endif; ?>
                        </div>
                        <div id="email-div" class="hidden <?php if (isset($_SESSION['emailErrorInvalid']) || isset($_SESSION['emailErrorInUse'])){echo'invalid-submit';}?>">
                            <?php if (isset($_SESSION['emailErrorInvalid'])): ?>
                                <p><?php echo $_SESSION['emailErrorInvalid']; ?></p>
                            <?php endif; ?>
                            <?php if (isset($_SESSION['emailErrorInUse'])): ?>
                                <p><?php echo $_SESSION['emailErrorInUse']; ?></p>
                            <?php endif; ?>
                        </div>
                        <label class="pwd-input-label">Password</label>
                        <div class="input-wrap password-field-wrap">
                            <input required type="password" name="password" id="pwd1" class="user-input password-input <?php if (isset($_SESSION['passwordInvalid'])): ?>invalid-input<?php endif; ?>" placeholder="Your password here...">
                            <div class="icon-wrap">
                                <i id="pwd1-icon-cross" class="inactive-cross-icon fas fa-exclamation-circle"></i>
                            </div>
                            <div class="icon-wrap">
                                <i id="pwd1-icon-check" class="inactive-check-icon fas fa-check"></i>
                            </div>
                            <?php if (isset($_SESSION['passwordInvalid'])): ?>
                            <div class="icon-wrap">
                                <i id="pwd1-icon-cross" class="invalid-input-icon fas fa-exclamation-circle"></i>
                            </div>
                            <?php endif; ?>
                        </div>
                        <div id="pwd1-div" class="hidden <?php if (isset($_SESSION['passwordInvalid'])){echo'invalid-submit';}?>">
                            <?php if (isset($_SESSION['passwordInvalid'])): ?>
                                <?php
                                    if (isset($_SESSION['passwordInvalid_min_character'])){
                                        echo $_SESSION['passwordInvalid_min_character'];
                                    }
                                    if (isset($_SESSION['passwordInvalid_upper_character'])){
                                        echo $_SESSION['passwordInvalid_upper_character'];
                                    }
                                    if (isset($_SESSION['passwordInvalid_lower_character'])){
                                    echo $_SESSION['passwordInvalid_lower_character'];
                                    }
                                    if (isset($_SESSION['passwordInvalid_num_character'])){
                                    echo $_SESSION['passwordInvalid_num_character'];
                                    }?>
                            <?php endif; ?>
                        </div>
                        <label class="pwd-input-label-2">Confirm Password</label>
                        <div class="input-wrap confirm-password-field-wrap">
                            <input required type="password" name="confirmpassword" id="pwd2" class="user-input confirm-password-input <?php if (isset($_SESSION['passwordNoMatch'])): ?>invalid-input<?php endif; ?>" placeholder="Confirm password...">
                            <div class="icon-wrap">
                                <i id="pwd2-icon-cross" class="inactive-cross-icon fas fa-exclamation-circle"></i>
                            </div>
                            <div class="icon-wrap">
                                <i id="pwd2-icon-check" class="inactive-check-icon fas fa-check"></i>
                            </div>
                            <?php if (isset($_SESSION['passwordNoMatch'])): ?>
                                    <div class="icon-wrap">
                                        <i id="pwd2-icon-cross" class="invalid-input-icon fas fa-exclamation-circle"></i>
                                    </div>
                            <?php endif; ?>
                        </div>
                        <div id="pwd2-div" class="hidden <?php if (isset($_SESSION['passwordNoMatch'])){echo'invalid-submit';}?>">
                            <?php if (isset($_SESSION['passwordNoMatch'])): ?>
                                <p><?php echo $_SESSION['passwordNoMatch']; ?></p>
                            <?php endif; ?>
                        </div>
                        <div class="submit-wrap">
                            <input type="hidden" name="register" value="create_account">
                            <input type="submit" class="signup-button" id="signup-button" value="Register">
                            <a href="./" class="register-button"><p>login</p></a>
                        </div>
                        <div class="forgot-password-wrap">
                            <a href="#" class="forgot-password-link">Forgot your password?</a>
                        </div>
                    </form>
                </div>
            </div>
            <div class="parent">
                <div class="child"></div>
            </div>
        </div>
    </div>



