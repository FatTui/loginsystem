						//******* Written by Eden Stocker  *******//
							//******* Copyright 2018 *******//
//******* You must have a license to use this code for any commercial or personal projects *******//


//////////////////// SETTINGS ///////////////////

// !! YOU MUST SET THE VARIABLES TO THE SAME VALUES IN THE "signup.php" FILE !! //
// password format checks //
var pwd2_check_config = true; // If set to false the confirm password input will not be checked for errors.
var min_character = true; // If set to true the password field will invalidate a password if it doesn't meet the minimum character amount.
var min_character_amount = 8; // The amount of characters required before the password is valid
var upper_character = true; // If set to true the password field will invalidate the password if it is missing an uppercase character.
var lower_character = true; // If set to true the password field will invalidate the password if it is missing a lower character.
var num_character = false; // If set to true the password field will invalidate the password if it is missing a number.



// Error message values //
var invalidEmailValue = "invalid email address."; // Error message associated with an invalid email
var emailTakenValue = "This email is already taken."; // Error message associated with an email already being taken by another account
var pwdError1Value = "Must contain at least one number."; // Error message associated with needing a number
var pwdError2Value = "Must contain at least one lowercase letter."; // Error message associated with needing a lowercase letter
var pwdError3Value = "Must contain at least one uppercase letter."; // Error message associated with needing an uppercase letter
var pwdError4Value = "Must contain be at least " + min_character_amount + " characters long."; // Error message associated with needing the password to be more than 6 characters long
var pwd2ErrorValue = "Passwords do not match."; // Error message associated with the confirm password not being the same as the first password


//////////////////// CODE ///////////////////
jQuery(document).ready(function($) {
	// Setting regex expressions
    var emailRegex= /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    var passwordRegex= /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,120}$/;
    var passwordRegex1= /^(?=.*\d)/;
    var passwordRegex2= /(?=.*[a-z])/;
    var passwordRegex3= /(?=.*[A-Z])/;
    var passwordRegex4= new RegExp("\.{" + min_character_amount + ",120}$");
	// Setting counter variables to 0, these stop the invalid error messages duplicating
	var counterEmail=0;
	var counterEmail_check=0;
	var counterPwd1_number=0;
	var counterPwd1_lower=0;
	var counterPwd1_upper=0;
	var counterPwd1_length=0;
	var pwd2_match=0;
	var submitBtnEmail=false;
	var submitBtnPwd1=false;
	var submitBtnPwd2=false;
	submitBtnController();
	// Display a green tick and underline when email is valid and correct
	function validEmail() {
		// Removes red underline.
		$("#email").removeClass("invalid-input").addClass("valid-input");
		// Disables red cross.
		$("#email-icon-cross").removeClass("invalid-input-icon").addClass("inactive-cross-icon");
		// Enables green check.
		$("#email-icon-check").removeClass("inactive-check-icon").addClass("valid-input-icon");	
		// Removes red error box.
		$("#email-div").addClass("hidden").removeClass("invalid-submit");
		// Removes invalid email error message.
		$("p").remove(".email-invalid");
		// Sets counterEmail to 0 to avoid duplication of the invalid error messages.
		counterEmail = 0;
		// Sets submitBtnEmail to true as input is valid
		submitBtnEmail=true;
		// Calls submitBtnController function to determine if form submit button should be disabled or enabled.
		submitBtnController();
	}
	// display a red cross and underline when email is invalid and incorrect
	function invalidEmail() {
		// Adds red underline.
		$("#email").removeClass("valid-input").addClass("invalid-input");
		// Disables green check.
		$("#email-icon-check").removeClass("valid-input-icon").addClass("inactive-check-icon");	
		// Enables red cross.
		$("#email-icon-cross").removeClass("inactive-cross-icon").addClass("invalid-input-icon");
		// Enables red error box.
		$("#email-div").addClass("invalid-submit").removeClass("hidden");
		// Sets submitBtnEmail to false as input is invalid
		submitBtnEmail=false;
		// Calls submitBtnController function to determine if form submit button should be disabled or enabled.
		submitBtnController();
		// hide error messages when input is empty
		if($("#email").val() == "") {
			$("#email-div").addClass("hidden").removeClass("invalid-submit");
			$("#email").removeClass("invalid-input");
			$("#email-icon-cross").removeClass("invalid-input-icon").addClass("inactive-cross-icon");
		}
		// warn user email is invalid
		if(emailRegex.test($("#email").val())){
			$("p").remove(".email-invalid");
			counterEmail = 0;
		} else if(counterEmail <=0){
			$("#email-div").append("<p class=\"email-invalid\">" + invalidEmailValue + "</p>");
			counterEmail ++;
		}
	}
	// display a green tick and underline when pwd1 is valid and correct
	function validPwd1() {
		$("#pwd1").removeClass("invalid-input").addClass("valid-input");
		$("#pwd1-icon-cross").removeClass("invalid-input-icon").addClass("inactive-cross-icon");
		$("#pwd1-icon-check").removeClass("inactive-check-icon").addClass("valid-input-icon");
		$("#pwd1-div").addClass("hidden").removeClass("invalid-submit");
		// Sets submitBtnPwd1 to true as input is valid
		submitBtnPwd1=true;
		submitBtnController();
		// Removes error messages
		$("p").remove(".pwd1-warning-number");
		counterPwd1_number = 0;
		$("p").remove(".letter-warning");
		counterPwd1_lower = 0;
		$("p").remove(".letter-warning-upper");
		counterPwd1_upper = 0;
		$("p").remove(".pwd1-warning-length");
		counterPwd1_length = 0;
	}
	// display a red cross and underline when pwd1 is invalid and incorrect
	function invalidPwd1() {
		$("#pwd1").removeClass("valid-input").addClass("invalid-input");
		$("#pwd1-icon-check").removeClass("valid-input-icon").addClass("inactive-check-icon");
		$("#pwd1-icon-cross").removeClass("inactive-cross-icon").addClass("invalid-input-icon");
		$("#pwd1-div").addClass("invalid-submit").removeClass("hidden");
		submitBtnPwd1 = false;
		submitBtnController();
		if ($("#pwd1").val() == "") {
			$("#pwd1-div").addClass("hidden").removeClass("invalid-submit");
			$("#pwd1").removeClass("invalid-input");
			$("#pwd1-icon-cross").removeClass("invalid-input-icon").addClass("inactive-cross-icon");
		}
		// warn user that password needs to containt at least one number.
        if (num_character == true) {
            if (passwordRegex1.test($("#pwd1").val())) {
                $("p").remove(".pwd1-warning-number");
                counterPwd1_number = 0;
            } else if (counterPwd1_number <= 0) {
                $("#pwd1-div").append("<p class=\"pwd1-warning-number\">" + pwdError1Value + "</p>");
                counterPwd1_number++;
            }
        } else {
            counterPwd1_number++;
        }
		// warn user that password needs to have at least one lowercase letter.
        if (lower_character == true) {
            if (passwordRegex2.test($("#pwd1").val())) {
                $("p").remove(".letter-warning");
                counterPwd1_lower = 0;
            } else if (counterPwd1_lower <= 0) {
                $("#pwd1-div").append("<p class=\"letter-warning\">" + pwdError2Value + "</p>");
                counterPwd1_lower++;
            }
        } else {
            counterPwd1_lower++;
        }
		// warn user that password needs to have at least one uppercase letter.
        if (upper_character == true) {
            if (passwordRegex3.test($("#pwd1").val())) {
                $("p").remove(".letter-warning-upper");
                counterPwd1_upper = 0;
            } else if (counterPwd1_upper <= 0) {
                $("#pwd1-div").append("<p class=\"letter-warning-upper\">" + pwdError3Value + "</p>");
                counterPwd1_upper++;
            }
        } else {
            counterPwd1_upper++;
        }
		// warn user that password needs to be at least 6 characters long.
        if (min_character == true) {
            if (passwordRegex4.test($("#pwd1").val())) {
                $("p").remove(".pwd1-warning-length");
                counterPwd1_length = 0;
            } else if (counterPwd1_length <= 0) {
                $("#pwd1-div").append("<p class=\"pwd1-warning-length\">" + pwdError4Value + "</p>");
                counterPwd1_length++;
            }
        } else {
            counterPwd1_length++;
        }
	}

	// display a green tick and underline when pwd2 is valid and correct
    if(pwd2_check_config == true) {
        function validPwd2() {
            $("#pwd2").removeClass("invalid-input").addClass("valid-input");
            $("#pwd2-icon-cross").removeClass("invalid-input-icon").addClass("inactive-cross-icon");
            $("#pwd2-icon-check").removeClass("inactive-check-icon").addClass("valid-input-icon");
            $("#pwd2-div").addClass("hidden").removeClass("invalid-submit");
            $("p").remove(".pwd2-warning-match");
            pwd2_match = 0;
            submitBtnPwd2 = true;
            submitBtnController();
        }
    } else {
        function validPwd2() {

        }
	}
	// display a red cross and underline when pwd2 is invalid and incorrect
    if(pwd2_check_config == true) {
		function invalidPwd2() {
			$("#pwd2").removeClass("valid-input").addClass("invalid-input");
			$("#pwd2-icon-check").removeClass("valid-input-icon").addClass("inactive-check-icon");
			$("#pwd2-icon-cross").removeClass("inactive-cross-icon").addClass("invalid-input-icon");
			$("#pwd2-div").addClass("invalid-submit").removeClass("hidden");
			submitBtnPwd2=false;
			submitBtnController();
			// hide error messages when input is empty
			if($("#pwd2").val() == "") {
				$("#pwd2-div").addClass("hidden").removeClass("invalid-submit");
				$("#pwd2").removeClass("invalid-input");
				$("#pwd2-icon-cross").removeClass("invalid-input-icon").addClass("inactive-cross-icon");
			}
			// warn user that passwords do not match.
			if (passwordRegex.test($("#pwd2").val()) && $("#pwd1").val() == $("pwd2").val()) {
				$("p").remove(".pwd2-warning-match");
				pwd2_match = 0;
			} else if (pwd2_match <= 0) {
				$("#pwd2-div").append("<p class=\"pwd2-warning-match\">" + pwd2ErrorValue + "</p>");
				pwd2_match++;
			}

		}
	}
	//validate email
	$('#email').on('input', function () {
		if (emailRegex.test($("#email").val())) {
			//checkMailStatus calls the function to talk to PHP in order to check if an email is already in use or not.
			checkMailStatus();
			function checkMailStatus(){
			var email=$("#email").val();// Get the value in field email.
			$.ajax({
			    type:'post',
			    url:'includes/checkEmails.php',// Gathers PHP file to connect to the databse.
			    data:{email: email},
			    success:function(msg){
			    var emailCheckResult = (msg); // Set the emailCheckResult variable to the result in php.
			    	// If email is taken display an error message.
					if(emailCheckResult != "" && counterEmail_check <= 0){
						$("#email-div").append("<p class=\"email-in-use\">" + emailTakenValue + "</p>");
						counterEmail_check ++;
						// Call function to make the email input invalid.
						invalidEmail();	
					} else {
						// If the email is not taken make sure the class to display an error is removed.
						$("p").remove(".email-in-use");	
						// Set counterEmail_check to 0 to avoid duplication of error messages.
						counterEmail_check = 0;
						// Call function to show input is valid.
						validEmail();	
					}   
			    }
			 });
			}    				
		} else {
			// Call function to show the input in invalid.
			invalidEmail();
			// If the email is not valid make sure the class to display an error is removed.
			$("p").remove(".email-in-use");	
			// Sets counterEmail_check to 0
			counterEmail_check = 0;
		}
	});

	// validate passwords
	$('#pwd1').on('input', function() {
	    if (num_character == true) {
            if (passwordRegex1.test($("#pwd1").val())) {
                passwordInvalid1 = false;
            } else {
                passwordInvalid1 = true;
            }
        } else {
            passwordInvalid1 = false;
        }
        if (lower_character == true) {
            if (passwordRegex2.test($("#pwd1").val())) {
                passwordInvalid2 = false;
            } else {
                passwordInvalid2 = true;
            }
        } else {
            passwordInvalid2 = false;
        }
        if (upper_character == true) {
            if (passwordRegex3.test($("#pwd1").val())) {
                passwordInvalid3 = false;
            } else {
                passwordInvalid3 = true;
            }
        } else {
            passwordInvalid3 = false;
        }
        if (min_character == true) {
            if (passwordRegex4.test($("#pwd1").val())) {
                passwordInvalid4 = false;
            } else {
                passwordInvalid4 = true;
            }
        } else {
            passwordInvalid4 = false;
        }

		if (passwordInvalid1 == false && passwordInvalid2 == false && passwordInvalid3 == false && passwordInvalid4 == false) {
			// Call validPwd1 when password is valid and meets all the criteria.
			if($("#pwd1").val() != 0) {
                validPwd1();
			} else {
                invalidPwd1();
			}
			if ($("#pwd1").val() == $("#pwd2").val() && $("#pwd1").val() != 0){
				// Call function to show pwd2 is valid when passwords match.
				validPwd2();
			} else if ($("#pwd1").val() != $("#pwd2").val() && $("#pwd1").val() != 0){
				 //Call function to show pwd2 is invalid as there is no match between the passwords.
				invalidPwd2();
			}
		} else {
			// Call functions to display invalid input fields.
			invalidPwd1();
			invalidPwd2();
		}	
	});

	$('#pwd2').on('input', function() {
        if($("#pwd2").val() == 0) {
            invalidPwd2();
        }
		if ($("#pwd1").val() == $("#pwd2").val()  && $("#pwd2").val() != 0){
			// Call function to show pwd2 is valid when passwords match.
			validPwd2();
		} else if ($("#pwd1").val() != $("#pwd2").val()  && $("#pwd2").val() != 0){
			// Call function to show pwd2 is invalid as there is no match between the passwords.
			invalidPwd2();
		}
	});
	// This function determines whether or not the submit button should be enabled or disabled. This is triggered by valid and invalid inputs.
	function submitBtnController() {
		if(submitBtnEmail == true && submitBtnPwd1 == true && submitBtnPwd2 == true) {
			enableSubmitBtn();
		} else {
			disableSubmitBtn();
		}
	}
	function disableSubmitBtn() {
		$("#signup-button").prop('disabled', true);
	}
	function enableSubmitBtn() {
		$("#signup-button").prop('disabled', false);
	}
});