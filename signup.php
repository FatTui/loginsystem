<?php
                            //******* Written by Eden Stocker  *******//
                                //******* Copyright 2018 *******//
//******* You must have a license to use this code for any commercial or personal projects *******//


include 'includes/helpers.inc.php';

//////////////////// SETTINGS ///////////////////
/// // !! YOU MUST SET THE VARIABLES THE VARIABLES TO THE SAME VALUE IN THE "password_regex.js" FILE !! //
// password format checks //
$pwd2_check_config = true; // If set to false the confirm password input will not be checked for errors.
$min_character = true; // If set to true the password field will invalidate a password if it doesn't meet the minimum character amount.
$min_character_amount = 8; // The amount of characters required before the password is valid
$upper_character = true; // If set to true the password field will invalidate the password if it is missing an uppercase character.
$lower_character = true; // If set to true the password field will invalidate the password if it is missing a lower character.
$num_character = false; // If set to true the password field will invalidate the password if it is missing a number.

//////////////////// CODE ///////////////////
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if ($_POST['email']!="" and $_POST['password']!="" and $_POST['confirmpassword']!="")
    {
        session_start();
        function validateForm() {
            // check if email format is correct
            function checkEmail() {
                if (preg_match('/^(([^<>()\\[\\]\\\\.,;:\\s@\\"]+(\\.[^<>()\\[\\]\\\\.,;:\\s@\\"]+)*)|(".+"))@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$/', $_POST['email']))
                {
                    unset($_SESSION['emailErrorInvalid']);
                    $_SESSION['emailValid'] = true;
                    // check if email already exists in database
                    include './includes/checkEmails.php';
                    if($echeck==0)
                    {
                        unset($_SESSION['emailErrorInUse']);
                        $_SESSION['emailAvailable'] = true;
                        unset($echeck);
                    } else {
                        unset($_SESSION['emailAvailable']);
                        $_SESSION['emailErrorInUse'] = 'This email address is alreasy in use.';
                        unset($echeck);
                        return FALSE;
                    }
                } else {
                    unset($_SESSION['emailErrorInUse']);
                    unset($_SESSION['emailValid']);
                    $_SESSION['emailErrorInvalid'] = 'invalid email address.';
                    return FALSE;
                }
            }
            checkEmail();
            // validate password
            function checkPwd1() {
                $_SESSION['passwordValid'] = false;
                if ($GLOBALS["min_character"] == TRUE) {
                    if (preg_match('/.{'.$GLOBALS["min_character_amount"].',120}$/', $_POST['password'])) {
                        unset($_SESSION['passwordInvalid_min_character']);
                        $_SESSION['passwordValid_min_character'] = true;
                        $_SESSION['passwordValid'] = TRUE;
                    } else {
                        $_SESSION['passwordInvalid_min_character'] = '<p>At least ' . $GLOBALS["min_character_amount"] . ' characters</p>';
                        $_SESSION['passwordInvalid'] = TRUE;
                    }
                } else {
                    unset($_SESSION['passwordInvalid_min_character']);
                    $_SESSION['passwordValid_min_character'] = true;
                }
                if ($GLOBALS["upper_character"] == TRUE) {
                    if (preg_match('/(?=.*[A-Z])/', $_POST['password'])) {
                        unset($_SESSION['passwordInvalid_upper_character']);
                        $_SESSION['passwordValid_upper_character'] = true;
                        $_SESSION['passwordValid'] = TRUE;
                    } else {
                        $_SESSION['passwordInvalid_upper_character'] = '<p>At least one capital letter.</p>';
                        $_SESSION['passwordInvalid'] = TRUE;
                    }
                } else {
                    unset($_SESSION['passwordInvalid_upper_character']);
                    $_SESSION['passwordValid_upper_character'] = true;
                }
                if ($GLOBALS["lower_character"] == TRUE) {
                    if (preg_match('/(?=.*[a-z])/', $_POST['password'])) {
                        unset($_SESSION['passwordInvalid_lower_character']);
                        $_SESSION['passwordValid_lower_character'] = true;
                        $_SESSION['passwordValid'] = TRUE;
                    } else {
                        $_SESSION['passwordInvalid_lower_character'] = '<p>At least one lowercase letter.</p>';
                        $_SESSION['passwordInvalid'] = TRUE;
                    }
                } else {
                    unset($_SESSION['passwordInvalid_lower_character']);
                    $_SESSION['passwordValid_lower_character'] = true;
                }
                if ($GLOBALS["num_character"] == TRUE) {
                    if (preg_match('/^(?=.*\d)/', $_POST['password'])) {
                        unset($_SESSION['passwordInvalid_num_character']);
                        $_SESSION['passwordValid_num_character'] = true;
                        $_SESSION['passwordValid'] = TRUE;
                    } else {
                        $_SESSION['passwordInvalid_num_character'] = '<p>At least one number.</p>';
                        $_SESSION['passwordInvalid'] = TRUE;
                    }
                } else {
                    unset($_SESSION['passwordInvalid_num_character']);
                    $_SESSION['passwordValid_num_character'] = true;
                }
                if (!isset($_SESSION['passwordInvalid_min_character']) and
                    !isset($_SESSION['passwordInvalid_upper_character']) and
                    !isset($_SESSION['passwordInvalid_lower_character']) and
                    !isset($_SESSION['passwordInvalid_num_character']))
                {
                    unset($_SESSION['passwordInvalid']);
                    $_SESSION['passwordValid'] = true;
                }
            }
            checkPwd1();
            // check that the passwords match
            function checkPwd2() {
                if ($_POST['password'] == $_POST['confirmpassword'])
                {
                    unset($_SESSION['passwordNoMatch']);
                    $_SESSION['passwordsMatch'] = true;
                } else {
                    unset($_SESSION['passwordsMatch']);
                    $_SESSION['passwordNoMatch'] = 'passwords must match.';
                    return FALSE;
                }
            }
            checkPwd2();
        }
        validateForm();
        // Final check of input fields //
        if (isset($_SESSION['emailValid']) and
            isset($_SESSION['emailAvailable']) and
            isset($_SESSION['passwordValid']) and
            isset($_SESSION['passwordsMatch']))
        {
            $email = $_POST['email'];
            $password = md5($_POST['password'] . '#K_!243fhnHeda!!)*rt');
            include 'includes/db.inc.php';
            try
            {
                $sql = 'INSERT INTO users SET
            email = :email,
            password = :password';
                $s = $pdo->prepare($sql);
                $s->bindValue(':email', $email);
                $s->bindValue(':password', $password);
                $s->execute();
            }
            catch (PDOException $e)
            {
                $error = 'Error adding user.';
                include 'includes/error.html.php';
                exit();
            }
            unset($email);
            unset($password);
            session_unset();
            $_SESSION[''];
            header('Location: .');
            exit();
        } else {

        }
    }
}
include 'app/template/signuphead.php';
include 'app/pages/signup.php';
include 'app/template/scripts-signup.php';