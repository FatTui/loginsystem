<?php
// Connect to database.
include 'db.inc.php';
// Try the database.
try
  {
  	// SQL statement to retrieve email addresses.
    $sql = 'SELECT email FROM users WHERE
        email = :email';
    // Prepare the SQL.
    $s = $pdo->prepare($sql);
    // Bind the value :email to what was in the email input field.
    $s->bindValue(':email', $_POST['email']);
    // Execute the SQL statement.
    $s->execute();
    // Bind a variable to the database results.
	$echeck=$s->rowCount();
  }
// Catch any errors.
catch (PDOException $e)
  {
    $error = 'Error searching for users.';
    include 'error.html.php';
    exit();
  }
// Check if they were any matches to what the user entered, if yes then add text to be inserted into an error message.
if($echeck!=0)
{
	echo "<p style=\"display:none;\">Email address is already taken.<p>";
}