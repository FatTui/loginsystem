<?php
function userIsLoggedIn()
{
    if (isset($_POST['action']) and $_POST['action'] == 'log in')
    {
        $_SESSION['working'] = "here";
        if (!isset($_POST['email']) or $_POST['email'] == '' or
            !isset($_POST['password']) or $_POST['password'] == '')
        {
            $_SESSION['loginError'] = 'Please fill in both fields';
            return FALSE;
        }
        $password = md5($_POST['password'] . '#K_!243fhnHeda!!)*rt');
        if (databaseContainsUser($_POST['email'], $password))
        {
            session_start();
            $_SESSION['loggedIn'] = "";
            $_SESSION['email'] = $_POST['email'];
            $_SESSION['password'] = $password;
            unset($_SESSION['loginError']);
            return TRUE;
            header('Location: ./');
            exit();
        } else {
            session_start();
            unset($_SESSION['loggedIn']);
            unset($_SESSION['email']);
            unset($_SESSION['password']);
            $_SESSION['loginError'] = 'The specified email address or password was incorrect.';
            return FALSE;
        }
    }
    if (isset($_POST['action']) and $_POST['action'] == 'log out')
    {
        session_start();
        unset($_SESSION['loggedIn']);
        unset($_SESSION['email']);
        unset($_SESSION['password']);
        header('Location: ./');
        exit();
    }
    if (isset($_SESSION['loggedIn']))
    {
        return databaseContainsUser($_SESSION['email'], $_SESSION['password']);
    }
}
function databaseContainsUser($email, $password)
{
    include 'db.inc.php';
    try
    {
        $sql = 'SELECT COUNT(*) FROM users
        WHERE email = :email AND password = :password';
        $s = $pdo->prepare($sql);
        $s->bindValue(':email', $email);
        $s->bindValue(':password', $password);
        $s->execute();
    }
    catch (PDOException $e)
    {
        $error = 'Error searching for user.';
        include 'error.html.php';
        exit();
    }

    $row = $s->fetch();

    if ($row[0] > 0)
    {
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}

